\context Staff = "Violin" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Violín"
	\set Staff.shortInstrumentName = "Vl."
	\set Staff.midiInstrument = "Violin"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-Violin" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"

                \key a \major
		\transpose c c' {
		e 4. b cis' 4  |
		a 4 b 8 cis' ~ cis' 2  |
		d' 8 cis' a a ~ a 2  |
		d' 8 cis' a a ~ a 2  |
%% 5
		e 4. b cis' 4  |
		a 4 b 8 cis' ~ cis' 2  |
		d' 16 cis' a d' cis' a d' cis' a 2  |
		d' 16 cis' a d' cis' a d' cis' a 2  |
		b 4 cis' 8 d' ~ d' 2  |
%% 10
		a 4 b 8 cis' ~ cis' 4 cis' 16 b a b  |
		a 1  |
		R1  |
		r2 r8 a gis e  |
		cis 2 r8 a gis e  |
%% 15
		d 4. e 8 fis 4. gis 8  |
		a 4 a gis 2  |
		r8 a gis e cis 2  |
		r8 a gis e cis 2  |
		d 4 e fis gis  |
%% 20
		\time 2/4
		a 4 gis  |
		\time 4/4
		a 2. cis' 16 b a b  |
		a 2 r8 e a b  |
		a 2 r8 a gis e  |
		cis 2 r8 a gis e  |
%% 25
		d 4. e 8 fis 4. gis 8  |
		a 4 a gis 2  |
		r8 a gis e cis 2  |
		r8 a gis e cis 2  |
		d 4 e fis gis  |
%% 30
		\time 2/4
		a 4 gis  |
		\time 4/4
		a 2. r4  |
		b, 8 cis d e fis 4 gis  |
		a 4 cis e 4. e 8  |
		fis 4. e 8 fis 4 e  |
%% 35
		d 4. d 8 e 4 d  |
		cis 4. e 8 fis 2  |
		d 4 e fis d  |
		e 2 r8 e d cis  |
		a, 4 cis e 4. e 8  |
%% 40
		fis 4. e 8 fis 4 e  |
		d 4. d 8 e 4 d  |
		cis 4. e 8 fis 2  |
		d 4 e fis gis  |
		a 1  |
%% 45
		R1  |
		\bar "|."
		}
	}
>>
