\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		% intro
		a1 a1 d1 d1
		a1 a1 d1 d1
		b1:m d1 a1 a1

		% descansa en dios...
		a2 a2:/cis fis2:m cis2:m
		b2:m d2 e2:sus4 e2

		a2 a2:/cis fis2:m cis2:m
		b4:m cis4:m d2 e2
		a1 a1
	
		% descansa en dios...
		a2 a2:/cis fis2:m cis2:m
		b2:m d2 e2:sus4 e2

		a2 a2:/cis fis2:m cis2:m
		b4:m cis4:m d2 e2
		a1 b2:m e2

		% por eso hoy...
		a2 e2:/gis
		fis1:m
		d2 e2 cis2:m fis2:m
		b4:m cis4:m d2
		e2:sus4 e2

		% por eso hoy...
		a2 e2:/gis
		fis1:m
		d2 e2 cis2:m fis2:m
		b4:m cis4:m d2
		a1 a1
	}
