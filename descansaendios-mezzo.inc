\context Staff = "mezzsoprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "mezzosoprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble"

		\key a \major
		R1*12  |
		r8 a' gis' e' e' 2  |
		r8 a' gis' e' 16 e' ~ e' 4 r8 b  |
%% 15
		d' 8 cis' b a 16 d' ~ d' 8 cis' d' e'  |
		e' 2 r8 a' gis' e'  |
		e' 2 r8 a' gis' e' 16 fis' ~  |
		fis' 2 r  |
		b 8 cis' cis' d' d' 4 a'  |
%% 20
		\time 2/4
		a' 4 gis'  |
		\time 4/4
		a' 2. r4  |
		R1  |
		r8 a' gis' e' e' 2  |
		r8 a' gis' e' 16 e' ~ e' 4 r  |
%% 25
		d' 8 cis' b a 16 d' ~ d' 8 cis' d' e'  |
		e' 2 r8 a' gis' e'  |
		e' 2 r8 a' gis' e' 16 fis' ~  |
		fis' 2 r  |
		b 8 cis' cis' d' d' 4 a'  |
%% 30
		\time 2/4
		a' 4 gis'  |
		\time 4/4
		a' 2. r4  |
		r2 r4 r8 e'  |
		e' 4 cis' e' e' 8 e' 16 fis' ~  |
		fis' 8 cis' cis' b 16 cis' ~ cis' 4. r8  |
%% 35
		cis' 8 d' 16 d' ~ d' 8 e' e' 4. r8  |
		cis' 8 cis' 16 cis' ~ cis' 8 b cis' 4. r8  |
		b 8 cis' 16 cis' ~ cis' 8 d' d' 4 d' 8 d'  |
		e' 2. r8 e'  |
		e' 4 cis' e' e' 8 e' 16 fis' ~  |
%% 40
		fis' 8 cis' cis' b 16 cis' ~ cis' 4. r8  |
		cis' 8 d' 16 d' ~ d' 8 e' e' 4. r8  |
		cis' 8 cis' 16 cis' ~ cis' 8 b cis' 4. r8  |
		b 8 cis' cis' d' d' 4 e' 8 e' 16 cis' ~  |
		cis' 2. r4  |
%% 45
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "mezzosoprano" {
		Des -- can "sa en" Dios,
		sien -- te "su a" -- mor, __
		que to -- do lo que e -- res es de él.
		Des -- can "sa en" Dios,
		ha -- blán -- do -- le. __
		Al -- ma mí -- a, él te trans -- for -- mó.

		Des -- can "sa en" Dios,
		"que él" se que -- dó __
		en es -- te pe -- que -- "ño y" tier -- no pan
		Des -- can "sa en" Dios,
		en -- "tra en" su paz. __
		Al -- ma mí -- a, él te sal -- va -- rá.

		Por e -- so hoy con -- fia -- ré __ has -- "ta el" fi -- nal, __
		por -- que só -- "lo en" ti
		mul -- ti -- pli -- ca -- ré
		lo que tú __ hi -- cis -- te en mí.

		Por e -- so hoy "me a" -- li -- men -- to de tu pan, __
		por -- que só -- "lo a" -- sí
		me con -- ver -- ti -- rás
		en un ins -- tru -- men -- to de paz. __
	}
>>
